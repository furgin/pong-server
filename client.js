const grpc = require("grpc");
const protoLoader = require('@grpc/proto-loader');
const Victor = require('victor');

const PROTO_PATH = process.env.PROTO_PATH || `../pong-client/Assets/Network/Protocol.proto`;
const PONG_HOST = process.env.PONG_HOST || `localhost`;
const protocolDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const protocolDescriptor = grpc.loadPackageDefinition(protocolDefinition);
let playerName = process.argv[2].toUpperCase();
let currentPlayer = "unknown";

const pongClient = (() => {

    const client = new protocolDescriptor.pong.PongService(`${PONG_HOST}:50051`, grpc.credentials.createInsecure());
    const call = client.connect();
    call.write({connected: {name: playerName}});

    call.on('data', (message) => {
        // console.log(message);
        switch (message.payload) {
            case "newGame":
                const newGame = message.newGame;
                console.log(newGame);
                if (newGame.player1 === playerName) {
                    currentPlayer = "player1";
                }
                if (newGame.player2 === playerName) {
                    currentPlayer = "player2";
                }
                console.log("I am " + currentPlayer)
                break;
            case "gameState":
                const gameState = message.gameState;
                const player = gameState[currentPlayer];
                if (player) {

                    const paddlePosition = Victor.fromObject(player.position);
                    const paddleVelocity = Victor.fromObject(player.velocity);
                    const ballPosition = Victor.fromObject(gameState.ballPosition);
                    const ballVelocity = Victor.fromObject(gameState.ballVelocity);

                    if (ballVelocity.length() > 0) {

                        let diff = paddlePosition.x - ballPosition.x;
                        let direction = Math.sign(diff);
                        if (Math.sign(ballVelocity.x) == direction) {
                            console.log("ball is moving towards me");

                            let position = ballPosition;
                            let done = false;
                            let angle = ballVelocity.horizontalAngle();
                            let intersect;
                            let dist;

                            while (!done) {
                                let dist = Math.abs(paddlePosition.x - position.x);

                                // opp = dist * tan(angle)
                                let opp = dist * Math.tan(angle);
                                intersect = ballPosition.y + (direction * opp);

                                // predict new position after hitting wall
                                if (intersect < -4.5) {
                                    console.log("rebound on bottom wall")
                                } else if (intersect > 4.5) {
                                    opp = ballPosition.y - 4.5;
                                    // dist = opp / Math.sin(angle)
                                    dist = opp / Math.sin(angle);
                                    console.log("rebound on top wall at " + (ballPosition.x - dist))
                                    position = Victor.fromObject({x: ballPosition.x - dist, y: 4.5})
                                    angle = (Math.PI/2)-((Math.PI/2) - angle);
                                }

                                done = true;
                            }

                            // console.log("ball will intersect at " + intersect)
                            let moveDiff = intersect - paddlePosition.y;
                            let desiredPaddleDirection = Math.sign(moveDiff);
                            if (desiredPaddleDirection != Math.sign(paddleVelocity.y)) {
                                call.write({move: {direction: desiredPaddleDirection}})
                            }

                        } else {
                            // console.log("ball is moving away from me");
                            if (paddleVelocity.x != 0) {
                                call.write({move: {direction: 0}})
                            }
                        }

                    }
                } else {
                    console.log("cant find player")
                }
                break;
            case "connected":
                if (message.connected.name !== playerName) {
                    console.log("Issue game challenge to new player: " + message.connected.name)
                    call.write({challenge: {player1: playerName, player2: message.connected.name}});
                }
                break;
            case "challenge":
                call.write({accept: {player1: message.challenge.player1, player2: message.challenge.player2}});
                break;
        }
    });

    call.on('end', () => {
        console.log("stream: finished");
    });

    call.on('error', (e) => {
        console.log("error: ", e);
        setTimeout(pongClient, 1000);
    });

    // move back and forth
    // var direction = Math.sign(Math.random() - 0.5);
    // call.write({move: {direction: 1}})
    // setInterval(() => {
    //     call.write({move: {direction: 0}})
    // }, 500);

});


pongClient();
