FROM node:13.6
WORKDIR /usr/src/app
COPY package.json ./
COPY yarn.lock ./
RUN yarn install
COPY . /usr/src/app
RUN curl https://bitbucket.org/furgin/pong-client/raw/release/Assets/Network/Protocol.proto -o Protocol.proto

ENV PROTO_PATH=/usr/src/app/Protocol.proto
CMD ["yarn","server"]
