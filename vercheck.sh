#!/usr/bin/env bash

client_version=$1
server_version=$2

server_major=$(echo "$server_version" | cut -d. -f1)
server_minor=$(echo "$server_version" | cut -d. -f2)
server_patch=$(echo "$server_version" | cut -d. -f3)

client_major=$(echo "$client_version" | cut -d. -f1)
client_minor=$(echo "$client_version" | cut -d. -f2)
client_patch=$(echo "$client_version" | cut -d. -f3)

if [[ "${client_version}" != "${server_version}" ]]
then
  if [[ "${client_major}" != "${server_major}" ]]
  then
      echo "major";
  elif [[ "${client_minor}" != "${server_minor}" ]]
  then
      echo "minor";
  elif [[ "${client_patch}" != "${server_patch}" ]]
  then
      echo "patch";
  fi
else
  echo "same"
fi


