#!/usr/bin/env bash

server_version=$(semversioner current-version)
client_version=$(curl -s https://bitbucket.org/furgin/pong-client/raw/release/version)
change=$(./vercheck.sh "${client_version}" "${server_version}")
image="furgin/pong-server"

function update_version() {
  new_version=$1

  printf "module.exports = \"%s\"" "${new_version}" > src/version.js
  git add .
  git commit -m "Update files for new version '${new_version}' [skip ci]"
  git push origin

  # Tag
  git tag -a -m "Tagging for release ${new_version}" "${new_version}"
  git push origin "${new_version}"
}

function build_docker() {
  new_version=$1
  # build new docker image
  echo "${DOCKERHUB_PASSWORD}" | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
  docker build -t "${image}" .
  docker tag "${image}" "${image}:${new_version}"
  docker push "${image}"
}

if [[ "${change}" == "major" || "${change}" == "minor" ]]
then
  set -ex

  # generate new version
  semversioner add-change --type "${change}" --description "New client version ${client_version}"
  semversioner release
  new_version=$(semversioner current-version)
  update_version "${new_version}"
  build_docker "${new_version}"

else

  # Release server if patch changes exist
  semversioner release
  new_version=$(semversioner current-version)

  if [[ "${server_version}" != "${new_version}" ]]
  then
    echo "Server changes detected releasing..."
    update_version "${new_version}"
    build_docker "${new_version}"
  else
    echo "No changes detected..."
  fi

fi
