const express = require('express')
    , router = express.Router()

module.exports = (pongServer) => {
    router.get('/games', (req, res) => res.send(pongServer.games));
    router.get('/players', (req, res) => res.send(Object.values(pongServer.players).map(p => p.playerName)));
    return router;
}
