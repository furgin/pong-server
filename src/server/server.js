const grpc = require("grpc")
    , protoLoader = require('@grpc/proto-loader')
    , PongServer = require('./pongServer')
    , HttpServer = require('./httpServer')
    , fs = require('fs')
    , version = require('../version');

const server = new grpc.Server();

const PROTO_PATH = process.env.PROTO_PATH || `../pong-client/Assets/Network/Protocol.proto`;
const protocolDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const protocolDescriptor = grpc.loadPackageDefinition(protocolDefinition);

const pongServer = new PongServer(16, 9);
const httpServer = new HttpServer(3002, pongServer);

server.addService(protocolDescriptor.pong.PongService.service, {
    connect: function (call) {
        call.on('data', (message) => {
            switch (message.payload) {
                case "connected":
                    pongServer.emit('connected', call, message.connected);
                    httpServer.emit('connected', message.connected);
                    break;
                case "challenge":
                    pongServer.emit('challenge', call, message.challenge);
                    break;
                case "spectateRequest":
                    pongServer.emit('spectateRequest', call, message.spectateRequest);
                    break;
                case "accept":
                    pongServer.emit('accept', call, message.accept);
                    break;
                case "move":
                    pongServer.emit('move', call, message.move);
                    break;
                case "endGame":
                    pongServer.emit('endGame', call, message.endGame);
                    httpServer.emit('endGame', message.connected);
                    break;
            }
        });
        call.on('cancelled', () => {
            httpServer.emit('disconnected', {name: call.playerName.toUpperCase()});
            pongServer.emit('disconnected', call);
        });
        call.on('end', () => call.end());
    },
});

process.on('uncaughtException', function (err) {
    console.log('UNCAUGHT EXCEPTION:', err); // err.message is "foobar"
});

server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
console.log(`Server (${version}) running at http://0.0.0.0:50051`);
server.start();


process.on('SIGINT', () => {
    server.forceShutdown();
    // server.tryShutdown(()=>{
    //     console.log("Tried to shut diwn");
    // })
});

process.on('SIGTERM', () => {
    server.forceShutdown();
});

