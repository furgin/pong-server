const pl = require('planck-js')
    , gameloop = require('node-gameloop')
    , EventEmitter = require('events');

const fps = 60, timeStep = 1000 / fps;

function info(msg) {
    console.log("[INFO] " + msg);
}

function error(msg) {
    console.log("[ERROR] " + msg);
}

class PongServer extends EventEmitter {

    players = {};
    games = [];
    scale = 1.0;
    paddleSpeed = 4.0;

    broadcast = (message) => {
        Object.entries(this.players)
            .forEach(([_, call]) => {
                call.write(message);
            });
    };

    sendGameUpdate = (game) => {
        let ballVelocity = game.ball.getLinearVelocity();
        const gameState = {
            player1: {
                position: {
                    x: game.player1.paddle.getBody().getPosition().x / this.scale,
                    y: game.player1.paddle.getBody().getPosition().y / this.scale
                },
                velocity: {
                    x: game.player1.paddle.getBody().getLinearVelocity().x / this.scale,
                    y: game.player1.paddle.getBody().getLinearVelocity().y / this.scale
                },
                score: game.player1.score
            },
            player2: {
                position: {
                    x: game.player2.paddle.getBody().getPosition().x / this.scale,
                    y: game.player2.paddle.getBody().getPosition().y / this.scale
                },
                velocity: {
                    x: game.player2.paddle.getBody().getLinearVelocity().x / this.scale,
                    y: game.player2.paddle.getBody().getLinearVelocity().y / this.scale
                }, score: game.player2.score
            },
            ballPosition: {
                x: game.ball.getPosition().x / this.scale,
                y: game.ball.getPosition().y / this.scale,
            },
            ballVelocity: {
                x: ballVelocity.x / this.scale,
                y: ballVelocity.y / this.scale
            }
        };
        this.players[game.player1.name].write({gameState: gameState});
        this.players[game.player2.name].write({gameState: gameState});
        game.spectators.forEach(s => {
            this.players[s].write({gameState: gameState});
        })
    };

    constructor(w, h) {
        super();

        const width = w * this.scale
            , height = h * this.scale;

        this.on('connected', (call, connected) => {
            let playerName = connected.name.toUpperCase();
            info(`${playerName} connected`)
            if (!playerName || playerName.length < 2 || playerName.length > 10 || playerName.trim().length < 2) {
                error("invalid player: " + playerName);
                call.write({error: {message: "Invalid Name"}})
            } else if (this.players[playerName]) {
                error("duplicate player: " + playerName);
                call.write({error: {message: "Duplicate Player"}})
            } else {
                Object.keys(this.players)
                    .forEach((p) => {
                        call.write({connected: {name: p}});
                    });

                this.games
                    .forEach((g) => {
                        call.write({newGame: {player1: g.player1.name, player2: g.player2.name}});
                    });

                call.playerName = playerName;
                this.players[playerName] = call;
                this.broadcast({connected});
            }
        });

        this.on('challenge', (call, challenge) => {
            const player1 = challenge.player1.toUpperCase();
            const player2 = challenge.player2.toUpperCase();
            info(`${player1} challenged ${player2}`)

            if (!call.playerName || player1 !== call.playerName || !this.players[player1] || !this.players[player2]) {
                error("invalid challenge: ", challenge);
            } else {
                if (isInGame(player1) || isInGame(player2)) {
                    call.write({error: {message: "Player in game"}})
                } else {
                    this.players[player2].write({challenge});
                }
            }
        });

        const isInGame = (playerName) => this.games.some((g) => g.player1.name === playerName || g.player2.name === playerName)

        this.on('accept', (call, accept) => {
            const player1 = accept.player1.toUpperCase();
            const player2 = accept.player2.toUpperCase();

            info(`${player2} accepted challenge from ${player1}`)
            if (!call.playerName || player2 !== call.playerName || !this.players[player1] || !this.players[player2]) {
                error("invalid accept: ", accept);
                return;
            } else {
                this.players[player1].write({accept});
            }

            if (isInGame(player1) || isInGame(player2)) {
                error("player in game: ", accept);
                return;
            }

            // broadcast new game
            info("starting game: " + player1 + " vs " + player2);
            this.broadcast({newGame: {player1, player2}});

            const world = pl.World({
                gravity: pl.Vec2(0, 0)
            });

            const ballSize = 0.3 * this.scale;

            const ballFixDef = {friction: 0, restitution: 1}
                , ballBodyDef = {bullet: true, linearDamping: 0, angularDamping: 0}
                , wallFixDef = {friction: 0, restitution: 1}
                , paddleFixDef = {friction: 0, restitution: 1}
                , width2 = width / 2
                , height2 = height / 2;

            const ball = world.createDynamicBody(ballBodyDef);
            const ballFixture = ball.createFixture(pl.Circle(pl.Vec2(0, 0), ballSize), {
                ...ballFixDef,
                userData: "ball"
            });

            const paddle1 = world.createDynamicBody(pl.Vec2(-7.5, 0.0))
                .createFixture(pl.Box(0.3, 1.0), {
                    ...paddleFixDef,
                    userData: "player1"
                });
            const paddle2 = world.createDynamicBody(pl.Vec2(7.5, 0.0))
                .createFixture(pl.Box(0.3, 1.0), {
                    ...paddleFixDef,
                    userData: "player2"
                });

            paddle1.getBody().setKinematic();
            paddle2.getBody().setKinematic();

            const topWall = world.createBody().createFixture(pl.Edge(pl.Vec2(-width2, -height2), pl.Vec2(width2, -height2)), {
                ...wallFixDef,
                userData: "Bottom"
            });
            const rightWall = world.createBody().createFixture(pl.Edge(pl.Vec2(width2, -height2), pl.Vec2(width2, height2)), {
                ...wallFixDef,
                userData: "Right"
            });
            const leftWall = world.createBody().createFixture(pl.Edge(pl.Vec2(-width2, -height2), pl.Vec2(-width2, height2)), {
                ...wallFixDef,
                userData: "Left"
            });
            const bottomWall = world.createBody().createFixture(pl.Edge(pl.Vec2(-width2, height2), pl.Vec2(width2, height2)), {
                ...wallFixDef,
                userData: "Top"
            });

            const game = {
                timeSinceUpdate: 0,
                player1: {name: player1, paddle: paddle1, score: 0},
                player2: {name: player2, paddle: paddle2, score: 0},
                world,
                ball,
                update: true,
                speed: 6.0,
                countdown: 3,
                log: (msg) => {
                    info(`[${player1} vs ${player2}]: ${msg}`)
                },
                reset: true,
                spectators: [],
                resetBall: () => {
                    ball.setTransform(pl.Vec2(0, 0), 0);
                    ball.setLinearVelocity(pl.Vec2(0, 0));
                    game.speed = 6.0;
                    game.countdown = 3;
                    game.timer = 0;
                    game.player1.paddle.getBody().setTransform(pl.Vec2(-7.5, 0.0), 0);
                    game.player1.paddle.getBody().setLinearVelocity(pl.Vec2(0, 0));
                    game.player2.paddle.getBody().setTransform(pl.Vec2(7.5, 0.0), 0);
                    game.player2.paddle.getBody().setLinearVelocity(pl.Vec2(0, 0));
                }
            }

            world.on('begin-contact', (contact) => {
                const fixtureA = contact.getFixtureA();
                const fixtureB = contact.getFixtureB();
                const contactPaddle1 = (fixtureA === paddle1 || fixtureB === paddle1) ? paddle1 : undefined;
                const contactPaddle2 = (fixtureA === paddle2 || fixtureB === paddle2) ? paddle2 : undefined;
                const contactPaddle = (contactPaddle1 ? contactPaddle1 : (contactPaddle2 ? contactPaddle2 : undefined))
                const contactBall = (fixtureA === ballFixture || fixtureB === ballFixture) ? ballFixture : undefined;
                const contactLeft = (fixtureA === leftWall || fixtureB === leftWall) ? leftWall : undefined;
                const contactRight = (fixtureA === rightWall || fixtureB === rightWall) ? rightWall : undefined;
                const contactTop = (fixtureA === topWall || fixtureB === topWall) ? leftWall : undefined;
                const contactBottom = (fixtureA === bottomWall || fixtureB === bottomWall) ? rightWall : undefined;

                if (contactBall && contactPaddle) {
                    let ballPosition = contactBall.getBody().getPosition();
                    let paddleDistance = contactPaddle.getBody().getPosition();
                    let dy = paddleDistance.y - ballPosition.y;
                    contactBall.getBody().applyLinearImpulse(pl.Vec2(0.0, -dy * 10), contactBall.getBody().getPosition(), true)
                }

                if (contactBall && contactLeft) {
                    game.player2.score++;
                    game.scorer = game.player2.name;
                    game.reset = true;
                }
                if (contactBall && contactRight) {
                    game.player1.score++;
                    game.scorer = game.player1.name;
                    game.reset = true;
                }

                const winningScore = 3;
                if (game.player1.score > winningScore) {
                    game.winner = game.player1.name;
                }
                if (game.player2.score > winningScore) {
                    game.winner = game.player2.name;
                }

                if (contactBall && (contactTop || contactBottom)) {
                    let ballVelocity = game.ball.getLinearVelocity();
                    let minX = 5;
                    if (ballVelocity.x >= 0 && ballVelocity.x < minX) {
                        ballVelocity.x = minX;
                    }
                    if (ballVelocity.x < 0 && ballVelocity.x > -minX) {
                        ballVelocity.x = -minX;
                    }
                }

                if (contactBall) {
                    game.speed += 0.25;
                }

                game.update = true;
            });

            this.games.push(game);
        });

        this.on('move', (call, move) => {

            for (let i = 0; i < this.games.length; i++) {
                let game = this.games[i];
                let player;
                if (game.player1.name === call.playerName) {
                    player = game.player1;
                }
                if (game.player2.name === call.playerName) {
                    player = game.player2;
                }
                if (player && !game.reset) {
                    player.paddle.getBody().setLinearVelocity(pl.Vec2(0, this.paddleSpeed * this.scale * Math.sign(move.direction)));
                    game.update = true;
                }
            }

        });

        const endGamesFor = (playerName) => {
            info("end all games for: " + playerName);
            // left as a spectator
            this.games = this.games.map((g) => {
                g.spectators = g.spectators.filter(p => p != playerName)
                return g;
            });
            this.games.forEach((g) => {
                if (g.player1.name === playerName || g.player2.name === playerName) {
                    this.broadcast({endGame: {player1: g.player1.name, player2: g.player2.name}})
                }
            })
            this.games = this.games.filter((g) => {
                return g.player1.name !== playerName && g.player2.name !== playerName;
            });
        }

        this.on('disconnected', (call) => {
            let playerName = call.playerName;
            if (call.playerName && this.players[playerName]) {
                info("disconnect: " + playerName);
                // remove the player
                delete this.players[playerName];
                Object.entries(this.players)
                    .filter(([name, _]) => name !== playerName)
                    .forEach(([_, playerEntry]) => playerEntry.write(
                        {disconnected: {name: playerName}}
                    ));
                endGamesFor(playerName);
            }
        });

        this.on('endGame', (call, endGame) => {
            let playerName = call.playerName;

            const player1 = endGame.player1.toUpperCase();
            const player2 = endGame.player2.toUpperCase();

            if (call.playerName === player1 || call.playerName === player2) {
                info("end game: " + player1 + " vs " + player2);
                endGamesFor(player1);
                endGamesFor(player2);
            }
        })

        this.on('spectateRequest', (call, spectateRequest) => {
            let spectator = call.playerName;
            const playerName = spectateRequest.player.toUpperCase();
            if (call.playerName === spectator) {
                info("player " + spectator + " wants to spectate " + playerName);
                var game = this.games.find(g => g.player1.name === playerName || g.player2.name === playerName);
                if (game) {
                    game.spectators.push(spectator);
                    call.write({spectateAccept: {player1: game.player1.name, player2: game.player2.name}})
                } else {
                    error(`${playerName} is not in a game`)
                }
            }
        })

        gameloop.setGameLoop((delta) => {
            this.games.forEach((game) => {

                // update the game
                if (this.players[game.player1.name] && this.players[game.player2.name]) {

                    if (game.winner) {

                        game.log(`${game.winner} won`);
                        const endGame = {
                            player1: game.player1.name,
                            player2: game.player2.name,
                            message: game.winner + " WON"
                        }
                        this.players[game.player1.name].write({endGame: endGame});
                        this.players[game.player2.name].write({endGame: endGame});
                        endGamesFor(game.player1.name);
                        endGamesFor(game.player2.name);

                    } else {

                        if (game.reset) {
                            game.resetBall();
                            game.reset = false;
                            this.sendGameUpdate(game);
                            const countdown = {countdown: game.countdown};
                            this.players[game.player1.name].write({countdown: countdown});
                            this.players[game.player2.name].write({countdown: countdown});
                            game.spectators.forEach(s => {
                                this.players[s].write({countdown: countdown});
                            })
                            game.speed = game.speed + (game.player1.score + game.player2.score) * 0.25;
                        }

                        if (game.countdown > 0) {

                            if (!game.timer) {
                                game.timer = delta;
                            } else {
                                game.timer += delta;
                            }

                            if (game.timer >= 1.0) {
                                game.countdown--;
                                game.timer--;
                                const countdown = {countdown: game.countdown};
                                this.players[game.player1.name].write({countdown: countdown});
                                this.players[game.player2.name].write({countdown: countdown});
                                game.spectators.forEach(s => {
                                    this.players[s].write({countdown: countdown});
                                })
                            }

                            if (game.countdown === 0) {
                                if (game.scorer && game.scorer == game.player1.name) {
                                    game.ball.setLinearVelocity(pl.Vec2(-game.speed / this.scale, 5 * (Math.random() - 0.5)));
                                } else {
                                    game.ball.setLinearVelocity(pl.Vec2(game.speed / this.scale, 5 * (Math.random() - 0.5)));
                                }
                                game.update = true;
                                const countdown = {countdown: game.countdown};
                                this.players[game.player1.name].write({countdown: countdown});
                                this.players[game.player2.name].write({countdown: countdown});
                                game.spectators.forEach(s => {
                                    this.players[s].write({countdown: countdown});
                                })
                                game.timer = 0;
                            }
                        }

                        if (game.countdown === 0) {

                            game.world.step(delta / this.scale);
                            game.timeSinceUpdate += delta;

                            // check paddle bounds
                            let player1Body = game.player1.paddle.getBody();
                            let player2Body = game.player2.paddle.getBody();
                            const checkPlayerBounrs = (body) => {
                                if (body.getPosition().y < -h / 2) {
                                    body.setTransform(pl.Vec2(body.getPosition().x, -h / 2), 0);
                                    body.setLinearVelocity(pl.Vec2(0, this.paddleSpeed * this.scale));
                                    game.update = true;
                                }
                                if (body.getPosition().y > h / 2) {
                                    body.setTransform(pl.Vec2(body.getPosition().x, h / 2), 0);
                                    body.setLinearVelocity(pl.Vec2(0, -this.paddleSpeed * this.scale));
                                    game.update = true;
                                }
                            }
                            checkPlayerBounrs(player1Body);
                            checkPlayerBounrs(player2Body);

                            if (game.timeSinceUpdate > 5 || game.update) {

                                game.update = false;
                                game.timeSinceUpdate = 0;

                                let ballVelocity = game.ball.getLinearVelocity();

                                // limit the ball's speed
                                let speed = ballVelocity.length();
                                const desiredSpeed = game.speed;
                                if (speed > 0) {
                                    game.ball.setLinearVelocity(ballVelocity.mul(desiredSpeed / speed))
                                }

                                this.sendGameUpdate(game);
                            }

                        }

                    }

                } else {
                    this.games = this.games.filter(g => g !== game);
                }
            })
        }, timeStep);
    }


}

module.exports = PongServer;