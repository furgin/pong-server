const express = require('express')
    , app = express()
    , api = require('./api')
    , EventEmitter = require('events');

class HttpServer extends EventEmitter {

    connections = [];

    constructor(port, pongServer) {
        super();

        this.pongServer = pongServer;

        app.use('/api', api(pongServer))
        app.use(express.static('public'))
        app.use(express.static('dist'))

        const server = app.listen(port, () => console.log(`Pong httpServer listening at http://localhost:${port}`))

        const shutDown = () => {
            console.log('Received kill signal, shutting down gracefully');
            server.close(() => {
                console.log('Closed out remaining connections');
                process.exit(0);
            });

            setTimeout(() => {
                console.error('Could not close connections in time, forcefully shutting down');
                process.exit(1);
            }, 2000);

            this.connections.forEach(curr => curr.end());
            setTimeout(() => this.connections.forEach(curr => curr.destroy()), 1000);
        }

        server.on('connection', connection => {
            this.connections.push(connection);
            connection.on('close', () => this.connections = this.connections.filter(curr => curr !== connection));
        });

        process.on('SIGTERM', shutDown);
        process.on('SIGINT', shutDown);

        this.io = require('socket.io')(server);
        this.io.on('connection', (socket) => {
            console.log('a user connected');

            // send current list of players
            Object.entries(this.pongServer.players)
                .forEach(([name, _]) => {
                    this.io.emit('connected', {connected: {name}});
                });

            socket.on('disconnect', () => {
                console.log('user disconnected');
            });
        });

        this.on('connected', (connected) => {
            this.io.emit('connected', {connected});
        });

        this.on('disconnected', (disconnected) => {
            this.io.emit('disconnected', {disconnected});
        });

    }

}

module.exports = HttpServer