import React from 'react';
import {Socket} from 'react-socket-io';
import config from './config';

const version = require('../version');
const uri = config.socket.url;
const options = config.socket.url;

export class App extends React.Component {
    render() {
        return (
            <Socket uri={uri} options={options}>
                <div>version: {version}</div>
            </Socket>);
    }
}
