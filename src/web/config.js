const devConfig = {
    socket: {
        url: "http://localhost:3002",
        options: {transports: ['polling']}
    },
    web: {
        url: "http://localhost:3002"
    }
};

const prodConfig = {
    socket: {
        url: "https://pong.furgin.org",
        options: {}
    }
};

export default (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? devConfig : prodConfig;

